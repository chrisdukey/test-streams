var fixTimestamps = require('./fixTimestamps.js');
var Readable = require('stream').Readable;
var util = require('util');
var Fusion = require("stream-fusion");
var DataSet = require("seeljavascript");
var moment = require('moment');
var fs = require('fs');
var ws = fs.createWriteStream('data.csv');


var s1 = require('./sp418.js');
var s2 = require('./sp424.js');
var s1count = 0;
var s2count = 0;
var s1chunk = [];
var s2chunk = [];
var minLength = s1.length;

var s1stream = new Readable({
   objectMode: true,
   read() {}
 })

var s2stream = new Readable({
    objectMode: true,
    read() {}
  })


if (s2.length < minLength){
  minLength = s2.length;
}

var n1 = 0;
setInterval(function() {
    if (n1++ < minLength) {

        if (s1count==100) {

          s1chunk = fixTimestamps(s1chunk);

          for (var x in s1chunk) {
            s1stream.push(s1chunk[x]);
          }
          s1chunk = [];
          s1count = 0;
        }
        s1chunk.push(s1[n1]);
        s1count++;

    } else if (n1++ === minLength) {
        s1stream.push(null);
    }
}, 10);

var n2 = 0;
setInterval(function() {
    if (n2++ < minLength) {
        if (s2count==100) {

          s2chunk = fixTimestamps(s2chunk);

          for (var x in s2chunk) {
            s2stream.push(s2chunk[x]);
          }
          s2chunk = [];
          s2count = 0;
        }
        s2chunk.push(s2[n2]);
        s2count++;
    } else if (n2++ === minLength) {
        s2stream.push(null);
    }
}, 10);

// for (var s=0; s<=minLength; s++) {
//   if (s1count==100) {
//     console.log(s1chunk);
//     for (var x in s1chunk) {
//       s1stream.push(JSON.stringify(s1chunk[x]));
//     }
//     s1chunk = [];
//     s1count = 0;
//   }
//   s1chunk.push(s1[s]);
//   s1count++;
//
//   if (s2count==100) {
//     console.log(s2chunk);
//     for (var x in s2chunk) {
//       s2stream.push(JSON.stringify(s2chunk[x]));
//     }
//     s2chunk = [];
//     s2count = 0;
//   }
//   s2chunk.push(s2[s]);
//   s2count++;
//
// }
var tk1 = 0;
var tk2 = 0;
function testKey1(item) {
  console.log('Item1 ' + item.timestamp.toString());
  tk1++;
  return tk1;
}

function testKey2(item) {
  console.log('Item2 ' + item.timestamp.toString());
  tk2++;
  return tk2;
}

// important.The sensor with the earliest datetime should be the one with check set to true

// get a dataSet object
var dataSet = DataSet(1.0 / 100.0);


var fused = new Fusion(
  // Need check to be true on one stream so data is emitted
    {stream: s1stream, key:"timestamp", check: true},
    {stream: s2stream, key: "timestamp", check: true},
    // Options
    //{bufferLength: 1000, buffer: 200}
    //{bufferLength: 5000, buffer: 1}
);

var sameCount = 0;
var isCalibrated = false;

function getRow(item) {
  var rowArray = new Array(10);
  rowArray[0] = item.record;
  rowArray[1] = 0; // Type - not used
  rowArray[2] = new moment(item.timestamp);
  rowArray[3] = 0; // Time - not used
  rowArray[4] = item.gyro.x;
  rowArray[5] = item.gyro.y;
  rowArray[6] = item.gyro.z;
  rowArray[7] = item.accel.x;
  rowArray[8] = item.accel.y;
  rowArray[9] = item.accel.z;
  return rowArray;
}

fused.transform = function(streamData) {
  //console.log("Chris " + streamData[0][0].timestamp.toString() + " " + streamData[1][0].timestamp.toString() + " " + streamData[0][0].sensor.toString() + " " + streamData[1][0].sensor.toString());
  //console.log(getRow(streamData[0][0]).toString());
  if (streamData[0][0].timestamp == streamData[1][0].timestamp) {
    console.log('Count ' + sameCount);
    if (sameCount==3000) {
        dataSet.Calibrate(sameCount);
        isCalibrated = true;
    }
    if (isCalibrated) {
      dataSet.CalculateAlpha();
      //console.log(dataSet.alpha_gyr[sameCount] + ' ROM: ' + dataSet.alpha_acc_gyr[sameCount]);
      console.log(dataSet.alpha_acc_gyr[sameCount-1]);
      ws.write(sameCount-1 + "," + dataSet.alpha_acc_gyr[sameCount-1] + "\n");
  
    }
    dataSet.AppendRow(getRow(streamData[0][0]), getRow(streamData[1][0]))
    sameCount++;
  }

  // can push as many times as desired but only one item per push
};

fused.pipe(process.stdout);
