var moment = require('moment');

function round(number, increment) {
    return Math.floor((number) / increment ) * increment;
}

function fixTimestamps(s_unprocessed, sample_rate){
 var increment = 0;
  if (sample_rate==100){
    increment = 10;
  } else if (sample_rate==50) {
    increment = 20
  } else {
    throw new Error("Invalid sample rate selected, should be 50 or 100");
  }

 // Loop through the batch of timestamps
  for (var s in s_unprocessed) {
      s_unprocessed[s].Timestamp = round(s_unprocessed[s].Timestamp,  increment);
  }

  return s_unprocessed;
}

module.exports = fixTimestamps;
