const log = require('why-is-node-running')
var fixTimestamps = require('./fixTimestamps.js');
var Readable = require('stream').Readable;
var util = require('util');
var Fusion = require("stream-fusion");
var DataSet = require("seeljavascript");
var moment = require('moment');
var fs = require('fs');
var ws = fs.createWriteStream('out.csv');
var fs = require('fs');
var Papa = require('papaparse');

// The two csv files below contain sample data from two sensors worn above and below the knee
var lower = './data/Justin22Jan20Try2_F0-15_2020-01-22T11.29.32.490_F015EBE1A1CF_Combined_50.000Hz_1.4.4.csv';
var upper = './data/Justin22Jan20Try2_D3-39_2020-01-22T11.29.32.490_D339D207A458_Combined_50.000Hz_1.4.4.csv';

// Sample rate is the setting that we use when streaming data from the sensors. We're using 50Hz
var sample_rate = 50;

setTimeout(function () {
  log() // logs out active handles that are keeping node running
}, 100)

// When the file is a local file we need to convert to a file Obj.

// Read in the 'upper' csv file
var content = fs.readFileSync(upper, "utf8");
var s1;
Papa.parse(content, {
    header: true,
    delimiter: ",",
    transform: function(value, header) {
      // N.B. I'm reversing the format of the time here because it is backwards (i.e. ss:mm:hh) in the csv
      // file. This is because the Matlab version of the algorithm works using the backwards
      // format (it was originally designed for a different sensor)
       if (header == 'Time') {
//        return value;
        return value.substring(6, 8) + ':' + value.substring(3, 5) + ':' + value.substring(0, 2) ;
       } else {
        return value;
        }
    },
    complete: function(results) {
      //  console.log("Finished:", results.data);
    s1 = results.data;
    }
});

// Read in the 'upper' csv file
var content = fs.readFileSync(lower, "utf8");
var s2;
Papa.parse(content, {
    header: true,
    delimiter: ",",
    transform: function(value, header) {
      // N.B. I'm reversing the format of the time here because it is backwards (i.e. ss:mm:hh) in the csv
      // file. This is because the Matlab version of the algorithm works using the backwards
      // format (it was originally designed for a different sensor)
       if (header == 'Time') {
        return value.substring(6, 8) + ':' + value.substring(3, 5) + ':' + value.substring(0, 2) ;
       } else {
        return value;
        }
    },
    complete: function(results) {
      //  console.log("Finished:", results.data);
    s2 = results.data;
    }
});

var s1count = 0;
var s2count = 0;
var s1chunk = [];
var s2chunk = [];
var minLength = s1.length;

if (s2.length < minLength){
  minLength = s2.length;
}

// Creating two objects that will represent the stream from upper and lower sensors
var s1stream = new Readable({
   objectMode: true,
   read() {}
 })

var s2stream = new Readable({
    objectMode: true,
    read() {}
  })

console.log('Streams created');

var n1 = 0;
setInterval(function() {
    if (n1++ < minLength) {

        // When we have enough rows of data in the s1chunk array we then fix the
        // timestamps
        if (s1count==sample_rate) {
          // The timestamps may be slightly out of sync between sensors
          // The fixTimestamps function just ensures they match between the two sensors
          s1chunk = fixTimestamps(s1chunk, sample_rate);

          // Push the fixed sensor data into the stream
          for (var x in s1chunk) {
            // Can push as many times as desired but only one item per push
            s1stream.push(s1chunk[x]);
          }
          s1chunk = [];
          s1count = 0;
        }
        // Reading the sensor data into an array (s1chunk) that will be the same size
        // as the sample_rate e.g. 50Hz rate gives an array of size 50
        s1chunk.push(s1[n1]);
        s1count++;

    } else if (n1++ === minLength) {
        s1stream.push(null);
    }
}, 2);

var n2 = 0;
setInterval(function() {
    if (n2++ < minLength) {
        if (s2count==sample_rate) {

          s2chunk = fixTimestamps(s2chunk, sample_rate);

          for (var x in s2chunk) {
            s2stream.push(s2chunk[x]);
          }
          s2chunk = [];
          s2count = 0;
        }
        s2chunk.push(s2[n2]);
        s2count++;
    } else if (n2++ === minLength) {
        s2stream.push(null);
    }
}, 2);

// Create a dataSet object that will be used for the algorithm
var dataSet = DataSet(1.0 / sample_rate);

// Integration point is the number of rows we need before we can call the DataSet.Calibrate function
// This is directly related to how long we do the calibration moves for e.g. if we do 20 secs of
// movement plus 5 secs of standing still before and after the movement (30 secs in total) then at 50Hz sample
// rate the integrationPoint value is equal to 30 * 50 = 1500
var integrationPoint = 1500

// Fusion object is used to read in the two sensor streams (s1stream and s2stream) and align them using the Timestamp
var fused = new Fusion(
  // Need check to be true on one stream so data is emitted
    {stream: s1stream, key:"Timestamp", check: true},
    {stream: s2stream, key: "Timestamp", check: true},
    // Options
    //{bufferLength: 1000, buffer: 200}
    //{bufferLength: 5000, buffer: 1}
);

var currentCount = 0;
var isCalibrated = false;

// The getRow function is used to put the data into the correct format for the Calibrate.AppendRow
// function
function getRow(item) {
  var rowArray = new Array(10);
  rowArray[0] = item.Record;
  rowArray[1] = 0; // Type - not used
  rowArray[2] = new moment(item.Timestamp);
  rowArray[3] = 0; // Time - not used
  rowArray[4] = ((item.GyroX)*Math.PI/180); // Convert from degrees per second to radians per second
  rowArray[5] = ((item.GyroY)*Math.PI/180);
  rowArray[6] = ((item.GyroZ)*Math.PI/180);
  rowArray[7] = ((item.AccelX)*9.807); // Convert from g's to m/s/s
  rowArray[8] = ((item.AccelY)*9.807);
  rowArray[9] = ((item.AccelZ)*9.807);
  //console.log(item);
  return rowArray;
}


fused.transform = function(streamData) {

  if (streamData[0][0].Timestamp == streamData[1][0].Timestamp) {
    console.log('Count ' + currentCount);

    // Here we're waiting until we have enough samples before we call the Calibrate function
    if (currentCount==integrationPoint) {
        //console.log('g1: ' + dataSet.g_1);
        // Run calibration
        dataSet.Calibrate(currentCount);
        isCalibrated = true;
    }
    if (isCalibrated) {
      // Calculate the Range of Motion angle for this data point. Result is stored
      // in the DataSet.alpha_acc_gyr array (it's added to the end of the array)
      dataSet.CalculateAlpha();

      // Write the most recent angular value to the out.csv file
      ws.write(currentCount-1 + "," + dataSet.alpha_acc_gyr[currentCount-1] + "\n");

    }
    dataSet.AppendRow(getRow(streamData[0][0]), getRow(streamData[1][0]))
    currentCount++;
  }


};

fused.pipe(process.stdout);
