var Readable = require('stream').Readable;
var Transform = require('stream').Transform;

const stream = new Readable({
   objectMode: true,
   read() {}
 })

 setInterval(() => {
   stream.push({ time: new Date() })
 }, 1000)

 stream.pipe(Transform({
   objectMode: true,
   transform (file, encoding, cb) {
     this.push(JSON.stringify(file))
     cb()
   }
 })).pipe(process.stdout);
