var Readable = require('stream').Readable;
var util = require('util');
var through2 = require('through2');
var Transform = require('stream').Transform;

var s1 = require('./sensor1.js');
var s2 = require('./sensor2.js');

var minLength = s1.length;
//console.log(s1);

var s1stream = new Readable({
   objectMode: true,
   read() {}
 })

 const s2stream = new Readable({
    objectMode: true,
    read() {}
  })

if (s2.length < minLength){
  minLength = s2.length;
}

var n1 = 0;
setInterval(function() {
    if (n1 < minLength) {
        s1stream.push(s1[n1]);
        n1++;
    } else if (n1 == minLength) {
        s1stream.push(null);
    }
}, 1000);

var n2 = 0;
setInterval(function() {
    if (n2++ < minLength) {
        s2stream.push(s2[n2]);
    } else if (n2++ === minLength) {
        s2stream.push(null);
    }
}, 1000);

var jsonStream = through2.obj(function(file, encoding, cb) {
    this.push(file.record.toString())
    cb()
})

s1stream.pipe(jsonStream).pipe(process.stdout);

// s1stream.pipe(Transform({
//   objectMode: true,
//   transform (file, encoding, cb) {
//     this.push(JSON.stringify(file))
//     cb()
//   }
// })).pipe(process.stdout);
