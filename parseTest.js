var fs = require('fs');
var Papa = require('papaparse');
var file = './data/Robert1-16Oct19_MetaWearD3_2019-10-16T10.34.52.696_D339D207A458_Combined_100.000Hz_1.4.4.csv';
// When the file is a local file we need to convert to a file Obj.
//  This step may not be necissary when uploading via UI
var content = fs.readFileSync(file, "utf8");

var rows;
Papa.parse(content, {
    header: true,
    delimiter: ",",
    complete: function(results) {
        console.log("Finished:", results.data);
    rows = results.data;
    }
});
